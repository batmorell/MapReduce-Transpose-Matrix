package fr.ece.hadoop.MapReduce;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MatrixTransposeMapper extends Mapper<LongWritable, Text, LongWritable, MapWritable> {

    private Text word = new Text();

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        // Spliting csv file by ","
        String[] vals = value.toString().split(",");

        MapWritable map = new MapWritable();

        // Create key value couple for each element
        // with the following format [col, [row, val]]
        int col = 0;
        for(String v : vals) {
            int val = Integer.parseInt(v);
            map.put(key, new IntWritable(val));
            context.write(new LongWritable(col), map);
            col++;
        }
    }
}


