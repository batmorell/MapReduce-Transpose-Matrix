package fr.ece.hadoop.MapReduce;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

public class MatrixTransposeReducer extends Reducer<LongWritable, MapWritable, LongWritable, Text> {

    public void reduce(LongWritable key, Iterable<MapWritable> maps, Context context) throws IOException, InterruptedException {

        SortedMap<LongWritable,IntWritable> rowVals = new TreeMap<LongWritable,IntWritable>();

        // Iterate on maps to get each values into row
        for (MapWritable map : maps) {
            for(Entry<Writable, Writable>  entry : map.entrySet()) {
                rowVals.put((LongWritable) entry.getKey(),(IntWritable) entry.getValue());
            }
        }

        // Create a string by concatenation
        StringBuffer sb = new StringBuffer();
        for(IntWritable rowVal : rowVals.values()) {
            sb.append(rowVal.toString());
            sb.append(",");
        }
        // Write key / value into the context
        context.write(key, new Text(sb.toString()));
    }
}
